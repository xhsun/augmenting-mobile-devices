﻿using NetworkIt;
using PhoneSide.pages;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Media.SpeechRecognition;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace PhoneSide
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        Client client;
        Pages current;
        string clientName = "hannah3";
        private SpeechRecognizer recognizer;
        Dictionary<string, UserControl> apps = new Dictionary<string, UserControl>();
        //0 - home screen, 1 - call screen
        private ImageBrush[] app_bkg=new ImageBrush[2];
        public MainPage()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Required;
            hide_status_bar();
            init_vars();
        }

        void client_MessageReceived(object sender, NetworkItMessageEventArgs e)
        {
            switch (e.ReceivedMessage.Name)
            {
                case "voice":    //A5 - 0 - voice
                    get_voice();
                    break;
                case "select":  //space - B - select
                    switch_app(current.select());
                    break;
                case "back":    //A4 - 1 - back
                    switch_app(current.back());
                    break;
                case "up":      //D3 - S - up
                    current.move(0);
                    break;
                case "right":   //D4 - A - right
                    current.move(3);
                    break;
                case "down":    //D5 - W - down
                    current.move(1);
                    break;
                case "left":    //click - N - left
                    current.move(2);
                    break;
                case "f0":     //D0 - G - Front 0
                    front_key(0, current.front_0());
                    break;
                case "f1":     //D0 - F - Front 1
                    front_key(1, current.front_1());
                    break;
                case "f2":     //D0 - D - Front 2
                    front_key(2, current.front_2());
                    break;
                case "b0":     //left - C - back 0
                    switch_app(current.select());
                    break;
                case "b1":     //up - Z - back 1
                    get_voice();
                    break;
                case "b2":     //down - X - back 2
                    front_key(1, current.front_1());
                    break;
                case "in_call":
                    switch_app("incall");
                    break;
                case "in_msg":
                    switch_app("inmsg");
                    break;
                case "EXIT":
                    Application.Current.Exit();
                    break;  //fine, there will be a break
                default:
                    return;
            }
        }

        public async void switch_app(string command)
        {
            if (command == "doNothing")
                return;
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                this.container.Children.Clear();
            });
            switch (command)
            {
                case "call":
                    ((call_screen)apps["call"]).Start_call();
                    goto case "text";
                case "continue_call":
                    ((call_screen)apps["call"]).change_state("0.01");
                    command = "call";
                    goto case "text";
                case "home":
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { this.container.Background = app_bkg[0]; });
                    goto case "map";
                case "camera":
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { this.container.Background = new SolidColorBrush(Colors.Black); });
                    goto case "map";
                case "incall":
                case "text":
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { this.container.Background = app_bkg[1]; });
                    goto case "map";
                case "map":
                    this.current = (Pages) apps[command];
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>{this.container.Children.Add(apps[command]);});
                    break;
                case "inmsg":
                    this.current = (Pages)apps["home"];
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        this.container.Children.Add(apps["home"]);
                        this.hint.Visibility = Visibility.Visible;
                        this.hintMsg.Text = "New message";
                    });
                    await Task.Delay(1500);
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { this.hint.Visibility = Visibility.Collapsed; });
                    break;
                default:
                    command = "home";
                    goto case "home";
            }
        }

        void front_key(int number, string msg)
        {
            if (msg == "doNothing")
                return;
            if (number == 0)
            {
                switch_app("call");
            }
            else if (number == 1)
            {
                switch_app("text");
            }
            else
            {
                switch_app("map");
            }
        }

        private async void get_voice()
        {
            SpeechRecognitionResult result = await recognizer.RecognizeAsync();

            if (result.Status == SpeechRecognitionResultStatus.Success)
            {
                if (result.Confidence == SpeechRecognitionConfidence.Rejected)
                {   //didnt catch what user says
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        this.hint.Visibility = Visibility.Visible;
                        this.hintMsg.Text = "Cannot understand";
                    });
                    await Task.Delay(1000);
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { this.hint.Visibility = Visibility.Collapsed; });
                    return;
                }
                switch (result.Text)
                {
                    case "Call Mom.":
                        switch_app("call");
                        break;
                    case "Message Mom.":
                        switch_app("text");
                        break;
                    case "Reply.":
                        switch_app("text");
                        break;
                    default:
                        switch_app(current.voice(result.Text));
                        break;
                }
            }
            else
            {  //unable to recognize the language
                await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    this.hint.Visibility = Visibility.Visible;
                    this.hintMsg.Text = "Fail to recognize";
                });
                await Task.Delay(1000);
                await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { this.hint.Visibility = Visibility.Collapsed; });
            }
        }

        private void init_vars()
        {
            load_recognizer();
            //connect to client
            client = new Client(clientName, "581.cpsc.ucalgary.ca", 8000);
            client.Connected += client_Connected;
            client.Error += client_Error;
            client.MessageReceived += client_MessageReceived;

            //init screen background
            app_bkg[0] = new ImageBrush();
            Uri uri = new Uri("ms-appx:///Assets/background.png", UriKind.Absolute);
            app_bkg[0].ImageSource = new BitmapImage(uri);

            app_bkg[1] = new ImageBrush();
            uri = new Uri("ms-appx:///Assets/dark_bkg.png", UriKind.Absolute);
            app_bkg[1].ImageSource = new BitmapImage(uri);

            //init all user control
            apps.Add("home", new home_screen());
            apps.Add("call", new call_screen());
            apps.Add("text", new text_screen());
            apps.Add("incall", new incall_screen());
            apps.Add("map", new map_screen());
            apps.Add("camera", new camera_screen());
            //TODO: apps.Add("default", new default_screen());

            //display home screen
            current = (Pages) apps["home"];
            this.container.Background = app_bkg[0];
            this.container.Children.Add(apps["home"]);
        }

        private async void load_recognizer()
        {
            recognizer = new SpeechRecognizer();
            recognizer.StateChanged += recognizer_StateChanged;

            // Compile the dictation grammar that is loaded by default.
            await recognizer.CompileConstraintsAsync();
        }

        async void recognizer_StateChanged(SpeechRecognizer sender, SpeechRecognizerStateChangedEventArgs args)
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>{
            switch (args.State)
            {
                case SpeechRecognizerState.Idle: //idle state, no hint is needed
                    this.hint.Visibility = Visibility.Collapsed;
                    break;
                case SpeechRecognizerState.Capturing:
                    this.hint.Visibility = Visibility.Visible;
                    this.hintMsg.Text = "Listening";
                    break;
                case SpeechRecognizerState.Processing:
                    this.hint.Visibility = Visibility.Visible;
                    this.hintMsg.Text = "Thinking";
                    break;
            }
            });
        }

        private async void hide_status_bar()
        {
            StatusBar statusBar = Windows.UI.ViewManagement.StatusBar.GetForCurrentView();
            // Hide the status bar
            await statusBar.HideAsync();
        }

        void client_Connected(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Connected");
        }

        void client_Error(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("ERROR");
        }

        

        #region stuff
        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }
#endregion stuff
    }
}
