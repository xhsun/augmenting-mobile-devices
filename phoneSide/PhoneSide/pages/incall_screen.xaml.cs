﻿using System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace PhoneSide.pages
{
    public sealed partial class incall_screen : UserControl, Pages
    {
        bool isAnswer = false;
        public incall_screen()
        {
            this.InitializeComponent();
            isAnswer = true;
            highlights.Begin();
            answerLight.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// 0 - up, 1 - down, 2 - left, 3 - right
        /// </summary>
        /// <param name="direction"></param>
        async void Pages.move(int direction)
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
            switch (direction)
            {
                case 2:
                    isAnswer = false;
                    this.rejectLight.Visibility = Visibility.Visible;
                    this.answerLight.Visibility = Visibility.Collapsed;
                    break;
                case 3:
                    isAnswer = true;
                    this.answerLight.Visibility = Visibility.Visible;
                    this.rejectLight.Visibility = Visibility.Collapsed;
                    break;
            }
            });
        }
        string Pages.voice(string msg)
        {
            switch (msg)
            {
                case "Answer.":
                    reset();
                    return "continue_call";
                case "Ignore.":
                    reset();
                    return "home";
            }
            return "doNothing";
        }
        string Pages.select()
        {
            if (isAnswer)
            {
                reset();
                return "continue_call";
            }
            reset();
            return "home";   
        }


        string Pages.back()
        {
            reset();
            return "home";
        }
        string Pages.front_0() { return "default"; }
        string Pages.front_1() { return "default"; }
        string Pages.front_2() { return "default"; }

        async void reset()
        {
            isAnswer = true;
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                this.answerLight.Visibility = Visibility.Visible;
                this.rejectLight.Visibility = Visibility.Collapsed;
            });
        }
    }
}
