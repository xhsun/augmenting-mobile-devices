﻿using System;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace PhoneSide.pages
{
    public sealed partial class call_screen : UserControl, Pages
    {
        public call_screen()
        {
            this.InitializeComponent();
        }

        public async void Start_call()
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                this.callingState.Text = "Calling";
            });

            await Task.Delay(1000);

            change_state("0:01");
        }

        public async void change_state(string msg)
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                this.callingState.Text = msg;
            });
        }

        void Pages.move(int direction){}//nowhere to move
        string Pages.select(){return "home";}//only one button to press....
        string Pages.back() { return "home"; }

        string Pages.voice(string msg)
        {
            if (msg == "Cancel.")
            {
                return "home";
            }
            return "doNothing";
        }

        string Pages.front_0() { return "default"; }
        string Pages.front_1() { return "default"; }
        string Pages.front_2() { return "default"; }
    }
}
