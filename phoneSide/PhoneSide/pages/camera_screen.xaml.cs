﻿using System;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace PhoneSide.pages
{
    public sealed partial class camera_screen : UserControl, Pages
    {
        public camera_screen()
        {
            this.InitializeComponent();
        }

        void Pages.move(int direction){}//nay where to go
        string Pages.front_0() { return "default"; }
        string Pages.front_1() { return "default"; }
        string Pages.front_2() { return "default"; }
        string Pages.voice(string msg)
        {
            if (msg == "Take photo.")
                take_img();
            return "doNothing";
        }
        string Pages.select()
        {
            take_img();
            return "doNothing";
        }

        string Pages.back()
        {
            reset();
            return "home";
        }

        async void take_img()
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                this.before.Visibility = Visibility.Collapsed;
                this.after.Visibility = Visibility.Collapsed;
            });
            await Task.Delay(100);
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                this.after.Visibility = Visibility.Visible;
            });
        }

        async void reset()
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                this.before.Visibility = Visibility.Visible;
            });
        }
    }
}
