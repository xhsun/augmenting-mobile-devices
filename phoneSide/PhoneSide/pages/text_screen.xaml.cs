﻿using System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace PhoneSide.pages
{
    public sealed partial class text_screen : UserControl, Pages
    {
        public text_screen()
        {
            this.InitializeComponent();
        }

        void Pages.move(int direction) { }//nay where to go
        string Pages.front_0() { return "default"; }
        string Pages.front_1() { return "default"; }
        string Pages.front_2() { return "default"; }
        string Pages.voice(string msg)
        {
            if(msg=="Send.")
                send_text();
            put_msg(msg);
            return "doNothing";
        }
        string Pages.select()
        {
            send_text();
            return "doNothing";
        }

        string Pages.back()
        {
            reset();
            return "home";
        }

        private async void put_msg(string msg)
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { this.message.Text = msg; });
        }
        async private void send_text()
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                if (this.message.Text == "")
                    return;
                if(this.reply1.Visibility==Visibility.Collapsed && this.reply2.Visibility==Visibility.Collapsed){
                    this.reply1.Visibility = Visibility.Visible;
                    this.replyMsg1.Text = this.message.Text;
                    this.message.Text = "";
                    this.replyTime1.Text = DateTime.Now.ToString("HH:mm tt");
                }
                else if (this.reply1.Visibility == Visibility.Visible && this.reply2.Visibility == Visibility.Collapsed)
                {
                    this.reply2.Visibility = Visibility.Visible;
                    this.replyMsg2.Text = this.message.Text;
                    this.message.Text = "";
                    this.replyTime2.Text = DateTime.Now.ToString("HH:mm tt");
                }
                else if (this.reply1.Visibility == Visibility.Visible && this.reply2.Visibility == Visibility.Visible)
                {
                    this.reply2.Visibility = Visibility.Collapsed;
                    this.replyMsg1.Text = this.message.Text;
                    this.message.Text = "";
                    this.replyTime1.Text = DateTime.Now.ToString("HH:mm tt");
                }
            });
        }

        async void reset()
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                this.message.Text = "";
                this.reply1.Visibility = Visibility.Collapsed;
                this.reply2.Visibility = Visibility.Collapsed;
            });
        }
    }
}
