﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace PhoneSide.pages
{
    public sealed partial class map_screen : UserControl, Pages
    {
        public map_screen()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// 0 - up, 1 - down, 2 - left, 3 - right
        /// </summary>
        /// <param name="direction"></param>
        void Pages.move(int direction) {
            switch (direction)
            {
                case 0:
                    up_down(10);
                    break;
                case 1:
                    up_down(-10);
                    break;
                case 2:
                    left_right(10);
                    break;
                case 3:
                    left_right(-10);
                    break;
            }
        }

        string Pages.voice(string msg)
        {
            switch (msg)
            {
                case "Go to Northland Village.":
                    focus_place();
                    break;
                case "Zoom in":
                    zoom(10);
                    break;
                case "Zoom out.":
                    zoom(-10);
                    break;
            }
            return "doNothing";
        }

        string Pages.select()
        {
            return "doNothing";
        }

        string Pages.back()
        {
            reset();
            return "home";
        }

        string Pages.front_0()
        { //go to preset place
            focus_place();
            return "doNothing";
        }
        string Pages.front_1()
        { //zoom out
            zoom(-10);
            return "doNothing";
        }
        string Pages.front_2()
        { //zoom in
            zoom(10);
            return "doNothing";
        }

        async void zoom(int zoom)
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                this.map.Height = this.map.Height + zoom;
                this.map.Width = this.map.Width + zoom;
            });
        }

        async void up_down(int value)
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                Canvas.SetTop(this.map, value + Canvas.GetTop(this.map));
            });
        }

        async void left_right(int value)
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                Canvas.SetLeft(this.map, value + Canvas.GetLeft(this.map));
            });
        }

        async void focus_place()
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                Canvas.SetLeft(this.map, -574);
                Canvas.SetTop(this.map, -460);
                this.map.Height = 1078;
                this.map.Width = 1692;
            });
        }

        async void reset()
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                Canvas.SetLeft(this.map, 0);
                Canvas.SetTop(this.map, 0);
                this.map.Height = 774;
                this.map.Width = 1692;
            });
        }
    }
}
