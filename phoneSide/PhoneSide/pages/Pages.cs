﻿namespace PhoneSide.pages
{
    interface Pages
    {
        /// <summary>
        /// 0 - up, 1 - down, 2 - left, 3 - right
        /// </summary>
        /// <param name="direction"></param>
        void move(int direction);
        string voice(string msg);
        string select();
        string back();
        string front_0();
        string front_1();
        string front_2();
    }
}
