﻿using System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace PhoneSide.pages
{
    public sealed partial class home_screen : UserControl, Pages
    {
        private int row;
        private int column;
        private Image[,] apps = new Image[3, 6];
        private string[,] app_string = new string[3, 6];
        
        public home_screen()
        {
            this.InitializeComponent();

            //init counter
            row = 0;
            column = 0;
            //first row
            apps[0, 0] = weatherLight;
            app_string[0, 0] = "weather";
            apps[0, 1] = timeLight;
            app_string[0, 1] = "time";
            apps[0, 2] = mapLight;
            app_string[0, 2] = "map";
            //second row
            apps[1, 3] = camLight;
            app_string[1, 3] = "camera";
            apps[1, 4] = noteLight;
            app_string[1, 4] = "note";
            apps[1, 5] = setLight;
            app_string[1, 5] = "setting";
            //third row
            apps[2, 2] = callLight;
            app_string[2, 2] = "call";
            apps[2, 3] = textLight;
            app_string[2, 3] = "text";

            //begin animation
            highlight.Begin();
            weatherLight.Visibility = Visibility.Visible;
        }

        

        /// <summary>
        /// 0 - up, 1 - down, 2 - left, 3 - right
        /// </summary>
        /// <param name="direction"></param>
        async void Pages.move(int direction)
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                apps[row, column].Visibility = Visibility.Collapsed;
            });
            
            switch (direction)
            {
                case 0:
                    row = (row == 0) ? 0 : row - 1;
                    change_column();
                    break;
                case 1:
                    row = (row == 2) ? 2 : row + 1;
                    change_column();
                    break;
                case 2:
                    column = (column == 0) ? 0 : column - 1;
                    change_row();
                    break;
                case 3:
                    column = (column == 5) ? 5 : column + 1;
                    change_row();
                    break;
                default:
                    break;
            }
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                apps[row, column].Visibility = Visibility.Visible;
            });
        }

        string Pages.select(){return app_string[row, column];}
        string Pages.back() { return "doNothing"; }//no where to go
        string Pages.front_0() { return "default"; }
        string Pages.front_1() { return "default"; }
        string Pages.front_2() { return "default"; }
        string Pages.voice(string msg)
        {
            //System.Diagnostics.Debug.WriteLine(msg);
            switch (msg)
            {
                case "Open camera.":
                    return "camera";
                case "Open map.":
                    return "map";
            }
            return "doNothing";
        }


        private void change_column()
        {
            //hard code, but I don't think I need any flexability anyways
            if (apps[row, column] == null)
            {
                if (column > 2)
                {
                    column = (row == 0) ? 2 : 1;
                }
                else if (column < 2)
                {
                    column = (row == 1) ? 3 : 2;
                }
                else { column = 3; }
            }
        }

        private void change_row()
        {
            //hard code, but I don't think I need any flexability anyways
            if (apps[row, column] == null)
            {
                switch (row)
                {
                    case 0:
                        row = 1;
                        break;
                    case 1:
                        row = 2;
                        break;
                    case 2:
                        row=(column>2) ? 1 : 0;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
