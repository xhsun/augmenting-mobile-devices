# README #
Project 2: Physically Augmenting Mobile Devices

### What is this repository for? ###

This repository contains the prototype implementation of physically augmenting mobile devices.
For more detail on the design and walkthrough of physically augmenting mobile devices, please visit [my web portfolio](http://pages.cpsc.ucalgary.ca/~xhsun/work/bracer.html)

Note: this project make use of the Makey Makey for the buttons

### How do I get set up? ###

* You can clone or download the repository to access it from your local machine
* link for clone: git clone https://xhsun@bitbucket.org/xhsun/augmenting-mobile-devices.git
* link for download: [link](https://bitbucket.org/xhsun/augmenting-mobile-devices/get/8f529e833c71.zip)
* note: if you decide to download the repository, the link for download will provide you with a zip that contain the source code

####To Run The Program####

* Open your visual studio and from there open both computer side and phone side project. Once its loaded, run computer side and phone side (on a windows 8 phone) at same time to get it going.

### Contribution guidelines ###

* Author: Hannah Sun

### Who do I talk to? ###

* Hannah Sun: xhsun@ucalgary.ca