﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NetworkItWPF;

namespace Host
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Client client;
        string clientName = "hannah3";
        public MainWindow()
        {
            InitializeComponent();

            client = new Client(clientName, "581.cpsc.ucalgary.ca", 8000);
            client.Error += client_Error;
            client.Connected += client_Connected;

            this.KeyUp += OnKeyUp;
        }


        void OnKeyUp(object sender, KeyEventArgs e)
        {
            Message message=new Message("ERROR");
            switch (e.Key){
                case Key.Escape:
                    message = new Message("EXIT");
                    client.SendMessage(message);
                    Application.Current.Shutdown();
                    break;
                case Key.D0:    //A5 - 0 - voice
                    message = new Message("voice");
                    break;
                case Key.N:    //click - N - left
                    message = new Message("left");
                    break;
                case Key.D1:    //A4 - 1 - back
                    message = new Message("back");
                    break;
                case Key.S:     //D3 - S - up
                    message = new Message("up");
                    break;
                case Key.A:     //D4 - A - right
                    message = new Message("right");
                    break;
                case Key.W:     //D5 - W - down
                    message = new Message("down");
                    break;
                case Key.B:     //space - B - select
                    message = new Message("select");
                    break;
                case Key.G:     //D0 - G - Front 0
                    message = new Message("f0");
                    break;
                case Key.F:     //D0 - F - Front 1
                    message = new Message("f1");
                    break;
                case Key.D:     //D0 - D - Front 2
                    message = new Message("f2");
                    break;
                case Key.C:     //left - C - back 0
                    message = new Message("b0");
                    break;
                case Key.Z:     //up - Z - back 1
                    message = new Message("b1");
                    break;
                case Key.X:     //down - X - back 2
                    message = new Message("b2");
                    break;
                case Key.O:     //trigger incoming call event
                    message = new Message("in_call");
                    break;
                case Key.P:     //trigger incoming message event
                    message = new Message("in_msg");
                    break;
                default:
                    return;
            }
            client.SendMessage(message);
        }

        void client_Connected(object sender, EventArgs e)
        {
            Console.WriteLine("Connected");
        }

        void client_Error(object sender, EventArgs e)
        {
            Console.WriteLine("Error");
        }
    }
}
